// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

{
    "id": 2,
    "firstName": "Jenny",
    "lastName": "Sample",
    "email": "jsample@mail.com",
    "password": "jenny1234",
    "isAdmin": false,
    "mobileNo": "09849572631"
}

// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

{
    "id": 20,
    "userId": 2,
    "productID" : 30,
    "transactionDate": "09-20-2021",
    "status": "paid",
    "total": 2500
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}

{
    "id": 30,
    "name": "Electric Fan",
    "description": "Humidity controller.",
    "price": 2500,
    "stocks": 500,
    "isActive": true,
}

